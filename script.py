"""
Giovanni De Martino - gdm_92@hotmail.it

Write a function that finds the contiguous subarray (containing at least one number) which has the largest sum within an array read in input to the function.
The function should return the sub-array and the corresponding sum.
Which is the complexity of your solution? 
Can you do a solution with linear complexity?

Write a unit test for the function

"""

import sys
from interviewFunctions import InterviewFunctions

def main(array):

    if len(array) > 0:
        print("Array: ", array)
        #result = InterviewFunctions.maxSubArray(array)
        result = InterviewFunctions.maxSubArrayOptimal(array)
        print("Max Sum: ", result["sum"])
        print("Sub Array: ", result["subArray"])
    else:
        print("Array should contain at least one element")


if __name__ == "__main__":

    if len(sys.argv) == 2:
        arrS = sys.argv[1].split(",")
        arrI = [int(x) for x in arrS]
        sys.exit(main(arrI))
    else:
        print(
            "The script accept only 1 argument. Es. 1,2,-4,-2 (each number separated by comma)"
        )
        sys.exit(0)
