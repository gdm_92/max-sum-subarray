'''
maxSubArray:
        Si calcola la somma di tutti i possibili sub-array e si sceglie quello con valore più grande.
        Si memorizzano indice di inizio e fine per ottenere in uscita anche il sub-array.

        La complessità temporale di questo algoritmo è O(n^2) a causa del ciclo for innestato in un'altro ciclo for



maxSubArrayOptimal:
        L'algoritmo di Kadane offre una soluzione più intelligente in termini di complessità computazionale.
        Essendoci un unico ciclo for, la complessità computazionale è O(n)
'''

class InterviewFunctions:

    @staticmethod
    def maxSubArray(array):
        if(len(array) > 0):
            maxSum = array[0]
            sum = 0
            startIndex = 0
            endIndex = 0
            n = len(array)

            for i in range(0, n):
                sum = 0
                for j in range(i, n):
                    sum += array[j]
                    if sum > maxSum:
                        maxSum = sum
                        startIndex = i
                        endIndex = j

            return {"sum": maxSum, "subArray": array[startIndex : endIndex + 1]}
        else:
            return "Error"

    @staticmethod
    def maxSubArrayOptimal(array):
        if len(array) > 0:
            max_so_far = array[0]
            max_ending_here = array[0]

            startIndex = 0
            endIndex = 0

            for i in range(1, len(array)):
                if array[i] > max_ending_here + array[i]:
                    startIndex = i
                    max_ending_here = array[i]
                else:
                    max_ending_here = max_ending_here + array[i]

                if max_so_far < max_ending_here:
                    max_so_far = max_ending_here
                    endIndex = i

            return {
                "sum": max_so_far,
                "subArray": array[min(startIndex, endIndex) : (endIndex + 1)],
            }
        else: 
            return "Error"