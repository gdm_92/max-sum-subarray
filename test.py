import unittest
from interviewFunctions import InterviewFunctions


class TestFunctions(unittest.TestCase):

    def test_not_optimal_0(self):
        self.assertEqual(InterviewFunctions.maxSubArray([]), "Error", "The result shoudl be Error")

    def test_not_optimal_1(self):
        self.assertEqual(InterviewFunctions.maxSubArray([1, 2, 3]), {
            "sum": 6,
            "subArray": [1,2,3],
        }, "Sum should be 6. Array should be [1, 2, 3]")

    def test_not_optimal_2(self):
        self.assertEqual(InterviewFunctions.maxSubArray([1]), {
            "sum": 1,
            "subArray": [1],
        }, "Sum should be 1. Array should be [1]")

    def test_not_optimal_3(self):
        self.assertEqual(InterviewFunctions.maxSubArray([1, -2, 3]), {
            "sum": 3,
            "subArray": [3],
        }, "Sum should be 3. Array should be [3]")

    def test_not_optimal_4(self):
        self.assertEqual(InterviewFunctions.maxSubArray([1, -1, 4, -4, 2, -6, 8, -1, 2, 1, -2, -1, 3]), {
            "sum": 10,
            "subArray": [8, -1, 2, 1],
        }, "Sum should be 11. Array should be [8, -1, 2, 1]")


    
    #test optimal solution
    def test_not_optimal_0(self):
        self.assertEqual(InterviewFunctions.maxSubArrayOptimal([]), "Error", "The result shoudl be Error")

    def test_optimal_1(self):
        self.assertEqual(InterviewFunctions.maxSubArrayOptimal([1, 2, 3]), {
            "sum": 6,
            "subArray": [1,2,3],
        }, "Sum should be 6. Array should be [1, 2, 3]")

    def test_optimal_2(self):
        self.assertEqual(InterviewFunctions.maxSubArrayOptimal([1]), {
            "sum": 1,
            "subArray": [1],
        }, "Sum should be 1. Array should be [1]")

    def test_optimal_3(self):
        self.assertEqual(InterviewFunctions.maxSubArrayOptimal([1, -2, 3]), {
            "sum": 3,
            "subArray": [3],
        }, "Sum should be 3. Array should be [3]")

    def test_optimal_4(self):
        self.assertEqual(InterviewFunctions.maxSubArrayOptimal([1, -1, 4, -4, 2, -6, 8, -1, 2, 1, -2, -1, 3]), {
            "sum": 10,
            "subArray": [8, -1, 2, 1],
        }, "Sum should be 11. Array should be [8, -1, 2, 1]")


if __name__ == '__main__':
    unittest.main()