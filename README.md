# max-sum-subarray

## Name
Max Sum Sub-array

## Usage
To run the script use the command "python .\script.py argument" where argument can be written as "1,-3,4" (each number divided by comma)
To run the test use the command "python .\test.py"
